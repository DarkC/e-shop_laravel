<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin', 'AdminViewController@index');

Route::get('/client', 'ClientViewController@index');

Route::get('/shop', 'ProductsClientController@index');

Route::get('/shop/{id}', 'ProductsClientController@byCategory');

Route::get('/show/{id}', 'ProductsClientController@show');

Route::resource('/categories', 'CategoryController');

Route::resource('/cart', 'CartController');

Route::resource('/products', 'ProductController');
