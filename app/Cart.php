<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'id_user', 'id_product', 'quantity', 'name',
    ];
}
