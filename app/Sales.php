<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
	public $timestamps = false;
    protected $fillable = [
        'user_id', 'price', 'name', 'description', 'image', 'quantity',
    ];
}
