<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Console\Command;

use Illuminate\Support\Facades\DB;

use Mail;
use App\Http\Sales;
use App\Http\User;
use Illuminate\Support\Facades\Auth;
class AdminViewController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    public function index()
    {
        
        $users = DB::table('users')->count();
       

       
        
    	$salesCount = DB::table('sales')->sum('quantity');
        $salesAmount = DB::table('sales')->sum('price');
        
        
        return view('admin/administratorView',compact(['users','salesCount','salesAmount']));
        
    }
}
