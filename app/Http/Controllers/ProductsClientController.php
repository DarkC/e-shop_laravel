<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductsClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $parents = \App\Category::where('id_parent', 0)->get();
        

        return view('client/categories',compact('parents'));
    }

    public function byCategory($id)
    {
    	$products = \App\Product::where('id_category', $id)->get();

    	return view('client/products', ['carts' => $products] );
    }

    public function show($id)
    {
        $products = \App\Product::find($id);

        return view('client/product',['products' => $products]);
    }
}
