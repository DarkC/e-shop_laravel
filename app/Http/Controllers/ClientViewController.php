<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Sales;
use Illuminate\Support\Facades\Auth;

class ClientViewController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$salesCount = DB::table('sales')->where('user_id', Auth::user()->id)->sum('quantity');
    	$salesAmount = DB::table('sales')->where('user_id', Auth::user()->id)->sum('price');
        return view('client/clientView',compact(['salesCount','salesAmount']));
    }
}
