<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'id_parent', 'name',
    ];

    public function getChields($id)
    {
        return \App\Category::where('id_parent', $id)->get();
    }
}
