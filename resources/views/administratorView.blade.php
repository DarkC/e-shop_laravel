@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
    <div class="col-md-12">
    
      <table class="table table-striped table-bordered">
        <tr>
          <th>Stadistic</th>
          <th>Quantity</th>
        </tr>
        <tr>
            <td>{{ __('Users Quantity') }}</td>
            <td>{{ $users }}</td>
        </tr>
        <tr>
            <td>{{ __('Total products selled') }}</td>
            <td>{{ $salesCount }}</td>
        </tr>
        <tr>
            <td>{{ __('Total amount of sales') }}</td>
            <td>{{ $salesAmount }}</td>
        </tr>
      </table>
    </div>
  </div>
</div>
@endsection
