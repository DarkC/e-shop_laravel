@extends('layouts.admin')

@section('content')
<div class="container">
  <form method="POST" action="{{ action('ProductController@update' , $product->id) }}" enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    <div class="form-group">
      <label>{{ __('Sku') }}</label>
      <input type="text" class="form-control" name="sku" value="{{ $product->sku }}" placeholder="Sku">
    </div>
    <div class="form-group">
      <label>{{ __('Name') }}</label>
      <input type="text" class="form-control" name="name" value="{{ $product->name }}" placeholder="Name">
    </div>
    <div class="form-group">
      <label>{{ __('Description') }}</label>
      <input type="text" class="form-control" name="description" value="{{ $product->description }}" placeholder="Description">
    </div>
    <div class="form-group">
      <label>{{ __('Image') }}</label>
      <input type="file" name="image" value="{{ $product->image }}" >
    </div>
    <div class="form-group">
      <label>Category</label>
      <select class="form-control" name="id_category">
        @foreach ($categories as $category) 
            <option value=" {{ $category->id }} " @if($product->id_category == $category->id) selected @endif>   {{ $category->name }}  </option>
        @endforeach
      </select>
    </div>
    <div class="form-group">
      <label>{{ __('Stock') }}</label>
      <input type="number" class="form-control" name="stock" value="{{ $product->stock }}" placeholder="Stock">
    </div>
    <div class="form-group">
      <label>{{ __('Price') }}</label>
      <input type="number" class="form-control" name="price" value="{{ $product->price }}" placeholder="Price">
    </div>
  <button type="submit" class="btn btn-dark">Update</button>
  </form>
</div>
@endsection