@extends('layouts.admin')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <table class="table table-striped table-bordered">
        <tr>
          <th>{{ __('Sku') }}</th>
          <th>{{ __('Name') }}</th>
          <th>{{ __('Description') }}</th>
          <th>{{ __('Image') }}</th>
          <th>{{ __('Id category') }}</th>
          <th>{{ __('Stock') }}</th>          
          <th>{{ __('Price') }}</th>
          <th class="text-right"><a class="btn bg-dark txt-light" href="{{ url('products/create')}}">{{ __('Create Category')}}</a></th>      
        </tr>
        @if($products)
          @foreach($products as $product)
          <td>{{$product->sku}}</td>
          <td>{{$product->name}}</td>
          <td>{{$product->description}}</td>
          <td>{{$product->image}}</td>
          <td>{{$product->id_category}}</td>
          <td>{{$product->stock}}</td>
          <td>{{$product->price}}</td>
          <td class="text-right">
                <a class="btn btn-success" href="{{ action('ProductController@edit', $product->id)}}">Edit</a>
                <form action="{{action('ProductController@destroy', $product->id)}}" method="POST">
                  @csrf
                    @method('DELETE')
                    <button type="summit" class="btn btn-danger">Delete</button>
                </form>
              </td>
          @endforeach
        @else
          <tr><td class="text-center" colspan="7">No data to display</td></tr>
        @endif
      </table>
    </div>
  </div>
</div>
@endsection