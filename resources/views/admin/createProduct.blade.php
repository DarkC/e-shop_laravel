@extends('layouts.admin')

@section('content')
<div class="container">
  <form method="POST" action="/products" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>{{ __('Sku') }}</label>
      <input type="text" class="form-control" name="sku" value="{{ old('sku') }}" placeholder="Sku">
    </div>
    <div class="form-group">
      <label>{{ __('Name') }}</label>
      <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name">
    </div>
    <div class="form-group">
      <label>{{ __('Description') }}</label>
      <input type="text" class="form-control" name="description" value="{{ old('description') }}" placeholder="Description">
    </div>
    <div class="form-group">
      <label>{{ __('Image') }}</label>
      <input type="file" name="image" value="{{ old('image') }}" >
    </div>
    <div class="form-group">
      <label>Category</label>
      <select class="form-control" name="id_category">
        @foreach ($categories as $category) 
            <option value=" {{ $category->id }} ">   {{ $category->name }}  </option>
        @endforeach
      </select>
    </div>
    <div class="form-group">
      <label>{{ __('Stock') }}</label>
      <input type="number" class="form-control" name="stock" value="{{ old('stock') }}" placeholder="Stock">
    </div>
    <div class="form-group">
      <label>{{ __('Price') }}</label>
      <input type="number" class="form-control" name="price" value="{{ old('price') }}" placeholder="Price">
    </div>
  <button type="submit" class="btn btn-dark">Create</button>
  </form>
</div>
@endsection