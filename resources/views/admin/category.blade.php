@extends('layouts.admin')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <table class="table table-striped table-bordered">
        <tr>
          <th>{{ __('Name of  category') }}</th>
          <th>{{ __('Parent Category') }}</th>
          <th class="text-right"><a class="btn bg-dark txt-light" href="{{ url('categories/create')}}">{{ __('Create Category')}}</a></th>
        </tr>
        @if(!empty($allCategories))
          @foreach($allCategories as $category)
            <tr>
              <td>{{$category->name}}</td>
              <td>{{$category->id_parent}}</td>
              <td class="text-right">
                <a class="btn btn-success" href="{{ action('CategoryController@edit', $category->id)}}">Edit</a>
                <form action="{{action('CategoryController@destroy', $category->id)}}" method="POST">
                  @csrf
                    @method('DELETE')
                    <button type="summit" class="btn btn-danger">Delete</button>
                </form>
              </td>
            </tr>
          @endforeach
        @else
          <tr>
            <td class="text-center" colspan="3">No data to display</td>
          </tr>
        @endif
      </table>
    </div>
  </div>
</div>
@endsection