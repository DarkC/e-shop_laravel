@extends('layouts.admin')

@section('content')
<div class="container">
  <form method="POST" action="{{ action('CategoryController@update' , $category->id) }}">
    @csrf
    @method('PATCH')
    <div class="form-group">
    <label>{{ __('Category Name') }}</label>
    <input type="text" class="form-control" name="name" value="{{ $category->name }}" placeholder="Category Names">
  </div>
  <div class="form-group">
    <label>Parent Category</label>
    <select class="form-control" name="id_parent">
      <option value='0'>Parent</option>
        @foreach ($parentCategories as $parent) 
            <option value=" {{ $parent->id }} " @if($category->id_parent == $parent->id) selected @endif>  {{ $parent->name }} 
            </option>
        @endforeach   
    </select>
  </div>
  <button type="submit" class="btn btn-dark">Update</button>
  </form>
</div>
@endsection