@extends('layouts.admin')

@section('content')
<div class="container">
  <form method="POST" action="/categories">
    @csrf
    <div class="form-group">
    <label>{{ __('Category Name') }}</label>
    <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Category Names">
  </div>
  <div class="form-group">
    <label>Parent Category</label>
    <select class="form-control" name="id_parent">
      <option value='0'>Parent</option>
        @foreach ($categories as $category) 
            <option value=" {{ $category->id }} ">   {{ $category->name }}  "</option>
        @endforeach   
    </select>
  </div>
  <button type="submit" class="btn btn-dark">Create</button>
  </form>
</div>
@endsection