@extends('layouts.client')

@section('content')
<div class="container">
  <form method="POST" action="/cart" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>{{ __('Image') }}</label><br> 
      <img src="{{URL::asset('/images/') . '/' .$products->image}}"  height="200" width="200">
    </div>
    <div class="form-group">
      <label>{{ __('Sku') }}</label>
      <input readonly type="text" class="form-control" name="sku" value="{{ $products->sku }}" placeholder="Sku">
    </div>
    <div class="form-group">
      <input type="hidden" class="form-control" name="id" value="{{ $products->id }}" placeholder="Sku">
    </div>
    <div class="form-group">
      <label>{{ __('Name') }}</label>
      <input readonly type="text" class="form-control" name="name" value="{{ $products->name }}" placeholder="Name">
    </div>
    <div class="form-group">
      <label>{{ __('Description') }}</label>
      <input readonly  type="text" class="form-control" name="description" value="{{ $products->description }}" placeholder="Description">
    </div>
    <div class="form-group">
      <label>{{ __('Image') }}</label>
      <input readonly type="text" class="form-control" name="image" value="{{ $products->image }}" placeholder="Description">
    </div>
    <div class="form-group">
      <label>Category</label>
      <input readonly type="text" class="form-control" name="category" value="{{ $products->id_category }}" placeholder="Description">
    </div>
    <div class="form-group">
      <label>{{ __('Quantity') }}</label>
      <input type="number" class="form-control" name="quantity" required placeholder="Quantity">
    </div>
    <div class="form-group">
      <label>{{ __('Price') }}</label>
      <input readonly type="number" class="form-control" name="price" value="{{ $products->price }}" placeholder="Price">
    </div>
  <button type="submit" class="btn btn-dark">Add to wish list</button>
  </form>
</div>
@endsection