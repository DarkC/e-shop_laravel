@extends('layouts.client')

@section('content')
<div class="container">
    @foreach($parents as $parent)
    <ul class="list-group">
      <li class="list-group-item"><a href="/shop/{{$parent->id}}">{{$parent->name}}</a>
        @foreach($parent->getChields($parent->id) as $chield)
          <ul>
            <li><a href="/shop/{{$chield->id}}">{{$chield->name}}</a></li>
          </ul>
        @endforeach
      </li>
    </ul>
    @endforeach
</div>
@endsection
