@extends('layouts.client')

@section('content')
<div class="container">
  
  <div class="row">
    <div class="col-md-12">
      <table class="table table-striped table-bordered">
        <tr>
          <th>Name of product</th>
        </tr>
        @foreach ($fails as $fail) 
          <tr><td>The product called: {{$fail}}, can't be bought for lack of stock, please check the stock, the product was deleted from your cart</td></tr>
        @endforeach
      </table>  
    </div>
  </div>
  <a href="/cart" class="btn btn-success">Go to the cart</a>  
</div>
@endsection