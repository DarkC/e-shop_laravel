@extends('layouts.client')

@section('content')
<div class="container">
  
  <div class="row">
    <div class="col-md-12">
      <table class="table table-striped table-bordered">
        <tr>
          <th>Name of product</th>
          <th>Sku</th>
		  <th>Foto</th>
		  <th>idCategoria</th>
		  <th>Stock</th>
		  <th>Precio</th>
		  <th>Descripcion</th>
		  <th>Options</th>
        </tr>
        @if ($carts) 
            @foreach ($carts as $fila) 
                          
                <td>{{$fila->name}}</td>
                <td>{{$fila->sku}}</td>
                <td> <img src="{{URL::asset('/images/') . '/' .$fila->image}}"  height="200" width="200"></td>
                <td>{{$fila->id_category}}</td>
                <td>{{$fila->stock}}</td>
                <td>{{$fila->price}}</td>
                <td>{{$fila->description}}</td>
                <td class="text-right">
                <a href="/show/{{$fila->id}}" class="btn btn-success">Ver</a>
                </td>
             @endforeach

            @else 
            <td class="text-center" colspan=7>No hay datos</td>
        @endif
      </table>
    </div>
  </div>  
</div>
@endsection
