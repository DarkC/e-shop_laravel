@extends('layouts.client')

@section('content')
<div class="container">
  
  <div class="row">
    <div class="col-md-12">
      <table class="table table-striped table-bordered">
        <tr>
          <th>Id product</th>
          <th>Name</th>
		      <th>Quantity</th>
		    </tr>
        @if($carts) 
            @foreach($carts as $fila) 
                <tr>      
                <td>{{$fila->id_product}}</td>
                <td>{{$fila->name}}</td>
                <td>{{$fila->quantity}}</td>
                </tr>   
            @endforeach
        @else 
          <td class="text-center" colspan=7>No hay datos</td>
        @endif
      </table>
      @if(!$carts->isEmpty())
        <form action="{{action('CartController@destroy',  Auth::user()->id )}}" method="POST">
          @csrf
          @method('DELETE')
          <button type="summit" class="btn btn-success">Proceed to check out</button>
        </form>
      @endif
    </div>
  </div>  
</div>
@endsection
